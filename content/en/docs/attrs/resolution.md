---
defaults:
- '96.0'
flags:
- bitmap output
- svg
minimums:
- '0.0'
title: resolution
types:
- double
used_by: G
description: Synonym for [`dpi`](/docs/attrs/dpi/).
---
