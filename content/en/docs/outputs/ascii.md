---
title: ASCII
description: ASCII art imagery
format:
- ascii
---
Since Graphviz 13.0.0, if Graphviz was built with
[AAlib](https://en.wikipedia.org/wiki/AAlib) support, the output format `ascii`
will produce an [ASCII art](https://en.wikipedia.org/wiki/ASCII_art)
representation of the input graph.
