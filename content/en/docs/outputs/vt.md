---
Title: VT100
description: Terminal-based graph output
format:
- vt
- vt-24bit
- vt-4up
- vt-6up
- vt-8up
---
Since Graphviz 9.0.0, on non-Windows platforms the VT100 formats are available
to generate output in a style for consumption by a
[VT100](https://en.wikipedia.org/wiki/VT100)-like terminal:
  - `vt`: 2-pixels-per-terminal-cell in 3-bit color
  - `vt-24bit`: 2-pixels-per-terminal-cell in 24-bit color
  - `vt-4up`: 4-pixels-per-terminal-cell in monochrome
  - `vt-6up`: 6-pixels-per-terminal-cell in monochrome
  - `vt-8up`: 8-pixels-per-terminal-cell in monochrome

These formats use UTF-8 characters for representing pixels, and thus assume a
UTF-8-capable terminal and locale.
